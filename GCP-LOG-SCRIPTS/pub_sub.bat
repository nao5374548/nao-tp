@echo off

SET SUBSCRIPTION_NAME=realtimeAnalysis_sub

REM Démarrer la récupération des messages de l'abonnement
gcloud pubsub subscriptions pull --auto-ack %SUBSCRIPTION_NAME% --format="flattened(ackId,message.messageId,message.publishTime,message.attributes,message.data.decode(base64).decode(utf-8))" --limit=20



