import json
import paho.mqtt.client as mqtt
from google.cloud import pubsub_v1
import os
import logging
import ssl

logging.basicConfig(level=logging.INFO)

os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "nao2-full-686622af978d.json"

mosquitto_broker_address = "172.31.253.77"
mosquitto_broker_port = 8883
mosquitto_topic = "meteo/france"

google_project_id = "nao2-full"
google_pubsub_topic = "Meteo_topic"

publisher = pubsub_v1.PublisherClient()
topic_path = publisher.topic_path(google_project_id, google_pubsub_topic)

def on_connect(client, userdata, flags, rc):
    logging.info(f"Connected to Mosquitto with result code {rc}")
    try:
        client.subscribe(mosquitto_topic)
        logging.info(f"Subscribed to MQTT topic: {mosquitto_topic}")
    except Exception as e:
        logging.error(f"Error subscribing to MQTT topic: {e}", exc_info=True)

def on_message(client, userdata, msg):
    try:
        payload = msg.payload.decode('utf-8')
        logging.info(f"Received message: {payload}")
        publish_to_pubsub(payload)
    except Exception as e:
        logging.error(f"Error processing MQTT message: {e}", exc_info=True)

def publish_to_pubsub(message):
    try:
        data = json.dumps({"message": message}).encode('utf-8')
        future = publisher.publish(topic_path, data)
        result = future.result(timeout=30)
        logging.info(f"Published to Google Cloud Pub/Sub: {result}")
    except Exception as e:
        logging.error(f"Error publishing to Google Cloud Pub/Sub: {e}", exc_info=True)

mqtt_client = mqtt.Client()

mqtt_client.tls_set(ca_certs=None, cert_reqs=ssl.CERT_NONE)

mqtt_client.on_connect = on_connect
mqtt_client.on_message = on_message

try:
    mqtt_client.connect(mosquitto_broker_address, mosquitto_broker_port, 60)
    logging.info("Connected to Mosquitto broker.")
except Exception as e:
    logging.error(f"Error connecting to Mosquitto broker: {e}")

mqtt_client.loop_start()

try:
    logging.info("Running... Press CTRL+C to exit")
    while True:
        pass
except KeyboardInterrupt:
    logging.info("Disconnected. Exiting...")
    mqtt_client.disconnect()
    mqtt_client.loop_stop()
except Exception as e:
    logging.error(f"Unexpected error: {e}")
    mqtt_client.disconnect()
    mqtt_client.loop_stop()