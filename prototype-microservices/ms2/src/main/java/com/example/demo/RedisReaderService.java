package com.example.demo;

import org.bson.Document;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Optional;

import org.springframework.data.domain.Sort;




@Service
public class RedisReaderService {

    @Autowired
    private StringRedisTemplate stringRedisTemplate;
    private final ModelRepository modelRepository;

    public RedisReaderService(ModelRepository modelRepository) {
        this.modelRepository = modelRepository;
    }

    @Autowired
    private MongoTemplate mongoTemplate;

    public String getN(String collectionName) {
        Query query = new Query();
        query.with(Sort.by(Sort.Direction.DESC, "_id"));
        query.limit(1);

        Document doc = mongoTemplate.findOne(query, Document.class, collectionName);
        if (doc != null && doc.containsKey("test2")) {
            String test2Content = doc.getString("test2");
            JSONObject jsonObject = new JSONObject(test2Content);
            return jsonObject.optString("nombre"); // Récupérer la valeur de 'nombre'
        }
        return null;
    }


    public void readCalculateAndPublish() {



        Optional<Model> m = modelRepository.findById(1L);
        int value = m.get().getSeuil();

        Map<String, String> map = new HashMap<>();
        map.put("average", getN("test2"));
        map.put("seuil", String.valueOf(value));

        stringRedisTemplate.opsForStream().add("targetStream", map);

    }
}
