package com.example.demo;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ScheduledTask {

    @Autowired
    private RedisReaderService redisReaderService;

    @Scheduled(fixedRate = 10000) //every 10 seconds
    public void control() throws Exception {
        redisReaderService.readAndCompare();
    }
}