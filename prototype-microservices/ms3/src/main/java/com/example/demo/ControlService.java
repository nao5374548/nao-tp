package com.example.demo;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;

@Service
public class ControlService {

    String status = "off";
    public void turnOn() {
        status = "on";

    }
    public void turnOff() {
        status = "off";

    }

    public String getStatus() {
        return status;

    }
}
