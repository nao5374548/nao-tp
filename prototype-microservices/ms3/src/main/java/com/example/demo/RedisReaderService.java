package com.example.demo;

import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Range;
import org.springframework.data.redis.connection.RedisZSetCommands;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.connection.stream.*;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Map;

@Service
public class RedisReaderService {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    private final ControlService controlService;

    public RedisReaderService(ControlService controlService) {
        this.controlService = controlService;
    }

    @Autowired
    private IMqttClient mqttClient;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    public void publishing(final String topic, final String payload) throws Exception {
        if (!mqttClient.isConnected()) {
            // Reconnect or handle connection issue
        }
        MqttMessage mqttMessage = new MqttMessage(payload.getBytes());
        mqttMessage.setQos(0);
        mqttMessage.setRetained(true);
        mqttClient.publish(topic, mqttMessage);
    }

    public void readAndCompare() throws Exception {

        List<MapRecord<String, Object, Object>> entries = stringRedisTemplate.opsForStream().reverseRange("targetStream", Range.unbounded(), RedisZSetCommands.Limit.limit().count(1));
        for (MapRecord<String, Object, Object> entry : entries) {

            Map<Object, Object> fields = entry.getValue();



            String v11 = (String) fields.get("average");
            String v22 = (String) fields.get("seuil");

            float v1 =  Float.parseFloat(v11);
            float v2 =  Float.parseFloat(v22);



            if (v1 > 0.68*v2) {
                controlService.turnOn();
                System.out.println(v1 > 0.68*v2);
                System.out.println(controlService.status);
                publishing("commands", "ON at " + System.currentTimeMillis());
                rabbitTemplate.convertAndSend("test3", "ON at " + System.currentTimeMillis());

            }

            else {
                controlService.turnOff();
                System.out.println(v1 < 0.68*v2);
                System.out.println(controlService.status);
                publishing("commands", "OFF at "+ System.currentTimeMillis());
                rabbitTemplate.convertAndSend("test3", "OFF at " + System.currentTimeMillis());


            }
        }

    }
}