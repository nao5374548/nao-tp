package com.example.demo;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@CrossOrigin("*")
@RestController
@RequestMapping(path = "/")
public class Control {

    private final ControlService controlService;

    String status = "off";

    public Control(ControlService controlService) {
        this.controlService = controlService;
    }

    @GetMapping("/on")
        public String turnOn() {
            controlService.turnOn();
            return status;

    }

    @GetMapping("/off")
    public String turnOff() {
        controlService.turnOff();
        return status;

    }

    @GetMapping("/status")
    public String getStatus() {
        return controlService.getStatus();

    }
}
