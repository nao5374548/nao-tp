package com.example.demo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisClusterConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;

@Configuration
public class RedisConfig {
    @Bean
    public LettuceConnectionFactory redisConnectionFactory() {

        RedisClusterConfiguration clusterConfig = new RedisClusterConfiguration();

        clusterConfig.clusterNode("192.168.1.9", 7000);
        clusterConfig.clusterNode("192.168.1.10", 7000);
        clusterConfig.clusterNode("192.168.1.14", 7000);
        return new LettuceConnectionFactory(clusterConfig);
    }
}
