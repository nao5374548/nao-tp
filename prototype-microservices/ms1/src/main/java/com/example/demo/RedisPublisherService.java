package com.example.demo;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StreamOperations;
import org.springframework.stereotype.Service;
import org.json.JSONObject;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Service
public class RedisPublisherService {

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Autowired
    private RabbitTemplate rabbitTemplate;
    ObjectMapper mapper = new ObjectMapper();


    @RabbitListener(queues = "test")
    public void receiveMessage(String message) throws Exception {
        JsonNode rootNode = mapper.readTree(message);
        ((ObjectNode) rootNode).fields();

        Map<String, String> map = new HashMap<>();
        map.put("id", String.valueOf(rootNode.get("id")));
        int i = Integer.parseInt(String.valueOf(rootNode.get("nombre"))) ;
        map.put("nombre", String.valueOf(i+0.1));

        redisTemplate.opsForStream().add("mystream", map);


        String jsonResult = mapper.writeValueAsString(map);
        rabbitTemplate.convertAndSend("test2", jsonResult);

    }
}
