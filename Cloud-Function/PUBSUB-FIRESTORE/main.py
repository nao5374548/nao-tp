from google.cloud import firestore
from datetime import datetime
import base64
import json

client = firestore.Client(project='nao2-full')

def pubsub_fire(event, context):
    print(f'This function was triggered by messageId {context.event_id}, published at {context.timestamp} to {context.resource["name"]}!')

    
    message = ''
    if 'data' in event:
        message = base64.b64decode(event['data']).decode('utf-8')
    print(f'message: {message}')


    try:
        data = json.loads(message)
    except json.JSONDecodeError as e:
        print(f'Error decoding JSON: {e}')
        return

  
    temperature = data.get('temperature', -1)
    humidity = data.get('humidity', -1)
    wind_speed = data.get('wind_speed', -1)
    precipitation = data.get('precipitation', -1)
    timestamp = data.get('timestamp', datetime.now().strftime("%Y-%m-%d %H:%M:%S"))

    print(f'temperature = {temperature}')
    print(f'humidity = {humidity}')
    print(f'wind_speed = {wind_speed}')
    print(f'precipitation = {precipitation}')
    print(f'timestamp = {timestamp}')

   
    doc_id = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    doc = client.collection('enviromentalSensors').document(doc_id)
    doc.set({
        'temperature': temperature,
        'humidity': humidity,
        'wind_speed': wind_speed,
        'precipitation': precipitation,
        'timestamp': timestamp
    })
